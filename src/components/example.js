const ExampleComponent = {};

ExampleComponent.template = function(text){
	return `<div style="background-color:orange">${text}</div>`;
}

ExampleComponent.render = function(){
	$(document.body).prepend(ExampleComponent.template("Ciao!"));
};

export default ExampleComponent;