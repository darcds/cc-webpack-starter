import ExampleComponent from "../components/example";
import setup from "../setup/setup";

export function init() {
	ExampleComponent.render();
}
