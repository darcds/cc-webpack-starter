// CONFIGURAZIONI

let filename = "nomeFile";
let cssIncluso = false; // se true il css viene incluso nel js

// FINE CONFIGURAZIONI

const fs = require('fs')
const CssMinimizerPlugin = require("css-minimizer-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = (env, argv) => {
  if (env.var === 'html') {
    cssIncluso = false;
  }
  let config = {
    module: {
      rules: []
    },
    plugins: [],
    externals: {
      jquery: "window.jQuery",
    },
    entry: {}
  };

  fs.readdirSync(__dirname + '/src').forEach(file => {

    if (file.startsWith('index')) {
      config.entry[filename + file.replace('index', '').replace('.js', '')] = { import: __dirname + '/src/' + file };
    }

  });

  if (argv.mode === "production") {
    config.output = {
      filename: "aem/[name].js",
    };
    config.optimization = {
      minimize: true,
      minimizer: [
        // For webpack@5 you can use the `...` syntax to extend existing minimizers (i.e. `terser-webpack-plugin`), uncomment the next line
        `...`,
        new CssMinimizerPlugin(),
      ],
    };
    config.plugins.push(
      new MiniCssExtractPlugin({
        filename: `aem/[name].css`,
      }));
  } else {
    // DEVELOPMENT
    config.output = {
      filename: `dev/[name].dev.js`,
    };

    config.plugins.push(
      new MiniCssExtractPlugin({
        filename: `dev/[name].dev.css`,
      }));
  }

  if (cssIncluso) {
    config.module.rules.push({
      test: /\.css$/i,
      use: [
        {
          loader: "style-loader",
        },
        {
          loader: "css-loader",
          options: { url: false },
        },
      ],
    });
  } else {
    config.module.rules.push({
      test: /\.css$/i,
      use: [
        {
          loader: MiniCssExtractPlugin.loader,
        },
        {
          loader: "css-loader",
          options: { url: false },
        },
      ],
    });
  }
  return config;
};