### Prerequisiti:
- [node & npm](https://nodejs.org/)

### Installa le dipendenze

Da lanciare la prima volta che si scarica la repo e ogni volta che viene aggiornata una dipendenza

```
npm install
```

### Configurazione nomi dei file e compilazione

Vedere webpack.config.js


### Comando NPM per sviluppare -> /dist/dev/[nomeFile].dev.js & /dist/dev/[nomeFile].dev.css 

È in watch: ricompila a ogni salvataggio di file.
```
npm run dev
```

### Comando NPM per produzione -> /dist/aem/[nomeFile].js & /dist/aem/[nomeFile].css

```
npm run build
```

### Comando NPM per generazione html inline -> /dist/target.html
Questo comando compila i file e li scrive in /dist/nomeFile_inline.html dentro i tag `<style>` e `<script>`
```
npm run inline
```

### VARIANTI
Se nella cartella src sono presenti più file che iniziano per "index" vengono usati come entry point per generare varianti.
es. index_b.js => /dist/nomeFile_b.js /dist/nomeFile_b.css /dist/nomeFile_inline_b.html


### Import supportati
- importa e trasforma la sintassi jsx di react
- importa file .css che vengono inclusi o estratti dal js in base alla configurazione

I moduli di react e jquery sono già installati.
